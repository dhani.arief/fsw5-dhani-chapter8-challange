import React, { Component } from 'react'

export default class Edit extends Component {
    submitForm = (e) => {
        e.preventDefault();
        
        this.props.handleData(this.state)
    }

    handleEmailChange = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    handleUsernameChange = (e) => {
        this.setState({
            username: e.target.value
        });
    }

    handlePasswordChange = (e) => {
        this.setState({
            password: e.target.value
        });
    }
    render() {
        return (
            <div>
                <form onSubmit={this.submitForm}>
                    <label for="email">Email:</label><br />
                    <input type="email" id="email" name="email" placeholder="Enter your e-mail" onChange={this.handleEmailChange}></input><br />
                    <label for="username">Username:</label><br />
                    <input type="text" id="username" name="username" placeholder="Enter your username" onChange={this.handleUsernameChange}></input><br />
                    <label for="password">Password:</label><br />
                    <input type="password" id="password" name="password" placeholder="Enter your password" onChange={this.handlePasswordChange}></input><br />
                    <input type="submit"></input>
                </form>
            </div>
        )
    }
}
