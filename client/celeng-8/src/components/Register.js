import React, { Component } from 'react'
// import { Route, Link } from 'react-dom'

export default class Register extends Component {
    constructor(props){
        super(props);

        this.state = {
            player: {
                email: "",
                username: "",
                password: ""
        }
    }}

    changeHandler = (e) =>{
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    submitHandler = (e) =>{
        const { email, username, password } = this.state
        alert(`
        E-mail: ${email}
        Username : ${username}
        Password : ${password}`)
    }
    render() {
        return (
            <div>
                <form onSubmit={this.submitHandler}>
                    <label for="email">Email:</label><br />
                    <input type="email" id="email" name="email" placeholder="Enter your e-mail" onChange={this.changeHandler}></input><br />
                    <label for="username">Username:</label><br />
                    <input type="text" id="username" name="username" placeholder="Enter your username" onChange={this.changeHandler}></input><br />
                    <label for="password">Password:</label><br />
                    <input type="password" id="password" name="password" placeholder="Enter your password" onChange={this.changeHandler}></input><br />
                    <input type="submit"></input>
                </form>
            </div>
        )
    }
}
