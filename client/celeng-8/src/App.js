import Home from "./views/Home";


function App() {
  return (
    <div class="App">
      <Home />
    </div>
  );
}

export default App;
